package com.dreampigsty.lib.drools.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(Drools)
public class DroolsAutoConfiguration {

}
