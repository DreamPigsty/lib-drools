package com.dreampigsty.lib.drools.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.drools")
public class DroolsProperties {
    
}
