package com.dreampigsty.lib.drools.common;

public class Suffix {
    public static final String SUFFIX_DRL = "drl";
    public static final String SUFFIX_EXCEL = "xls";
    public static final String SUFFIX_EXCEL_2007 = "xlsx";
    public static final String SUFFIX_CSV = "csv";
}
